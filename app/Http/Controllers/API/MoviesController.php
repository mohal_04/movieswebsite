<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Movie;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        // Get movies
        $movies = Movie::all(['imdbid']);
        if ($movies) {
            $guzzle_client = new Client();
            foreach ($movies as $movie) {
                $guzzle_response = $guzzle_client->request('GET', 'http://www.omdbapi.com?i=' . $movie->imdbid . '&apikey=b435a59d');
                if (200 == $guzzle_response->getStatusCode()) {
                    $data['data'][$movie->imdbid] = json_decode($guzzle_response->getBody(), true);
                }
            }
        }
        // return view('welcome', $data);
        return response()->json($data['data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
