<?php

namespace App\Http\Controllers;


class WelcomeController extends Controller
{
    /**
     * Homepage action
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        return view('welcome');
    }
}
