<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    /**
     * The model's default values for attributes.
     * @var array
     */
    protected $attributes = ['status' => 1];
    
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['imdbid', 'title', 'plot'];
}
