<?php

use Illuminate\Database\Seeder;
use App\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Movies array
        $movies = [
            [
                'imdbid' => 'tt8385474',
                'title' => "A Dog's Journey",
                'plot' => 'A dog finds the meaning of his own existence through the lives of the humans he meets.'
            ],
            [
                'imdbid' => 'tt1753383',
                'title' => "A Dog's Purpose",
                'plot' => 'A dog looks to discover his purpose in life over the course of several lifetimes and owners.'
            ],
            [
                'imdbid' => 'tt4154796',
                'title' => 'Avengers: Endgame',
                'plot' => "After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to reverse Thanos' actions and restore balance to the universe."
            ],
            [
                'imdbid' => "tt7721800",
                'title' => 'Bharat',
                'plot' => "At the cusp of India's birth as an Independent nation, a family makes an arduous journey to freedom at a cost. A young boy Bharat, makes a promise to his Father that he will keep his family together no matter what."
            ],
            [
                'imdbid' => "tt8752440",
                'title' => 'Batman: Hush',
                'plot' => "An adaptation of the Batman: Hush storyline, by Jeph Loeb and Jim Lee."
            ]
        ];
        
        foreach ($movies as $movie) {
            Movie::create($movie);
        }
    }
}
