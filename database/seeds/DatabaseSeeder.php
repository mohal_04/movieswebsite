<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // disable foreign key check
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        // Custom seeds array;
        $seeds = [
            [
                'question' => 'Seed movies?',
                'seeder'   => MoviesTableSeeder::class,
                'enabled'  => env('SEED_MOVIES', true)
            ]
        ];
        
        // Traverse seeds array
        foreach ($seeds as $seed) {
            if ($seed['enabled']) {
                if ($this->command->confirm($seed['question'])) {
                    $this->call($seed['seeder']);
                }
            }
        }
        // $this->call(UsersTableSeeder::class);
        
        // enable foreign key check
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
