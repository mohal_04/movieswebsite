# Movies Website

A small repository for test task.

## Quick Start

### Install Laravel packages
Run `composer install` to install Laravel package dependencies.

### Install NPM packages
Run `npm install` to install Vue.js package dependencies.

### Create **.env** file
Make a copy of **.env.example** file and rename it to **.env** in root directory.

### Database
Create a fresh database.
Name it **movieswebsite** or anything you want.

### **.env** file changes
Set **DB_DATABASE** to **movieswebsite** or anything you want.  
Set **DB_USERNAME** to **root** or anything you want.  
Set **DB_PASSWORD** to *any secure string.*  
Set **APP_NAME** to **MoviesWebsite.** (optional)  
Set **APP_URL** to your domain name or virtual host. (optional)  

Run `php artisan key:generate` to generate **APP_KEY.** You only need to do this if you get an error about an encryption key.  

### Run migrations
Run `php artisan migrate` to create tables.

### Regenerate composer autoloader
Run `composer dump-autoload` to regenerate composer autoloader.

### Seed (populate) Database
Run `php artisan db:seed` command to seed database.

### Run application
_Add virtual host if using Apache or run following command to execute application._

Run `php artisan serve`